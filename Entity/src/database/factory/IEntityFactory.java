package database.factory;

import connection.database.IDBConnection;
import persistence.database.Deletable;
import persistence.database.Insertable;
import persistence.database.Searchable;
import persistence.database.Updateable;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Alexander Komischke
 */
public interface IEntityFactory <T extends Insertable & Deletable & Updateable & Searchable> {

    /**
     * Parse a given ResultSet and returns a valid Object of type T. T is the type of the entity.
     *
     * @param databaseAnswer
     *         A ResultSet, which is an answer from the database for a given request.
     *
     * @return A valid Object, which has been parsed from the parameter.
     * @throws SQLException Error reported from the database.
     */
    T create( final ResultSet databaseAnswer )
            throws SQLException;

    /**
     * @param tableName Name of the table, which should be dropped.
     * @param idbConnection Connection used for dropping table
     *
     * @return How many lines have changed in the connection.database.
     *
     * @author Alexander Komischke
     * @throws SQLException Error reported from the database.
     */
    default int dropTable(
            final IDBConnection idbConnection,
            final String tableName )
            throws SQLException
    {
        return idbConnection.dropTable(tableName);
    }

    /**
     * @param idbConnection Connection used for creation of table
     * @param tableName Name of the table, which should be created.
     * @param tableValues Values schema for inserting
     *
     * @return How many lines have changed in the connection.database.
     *
     * @throws SQLException Error reported from the database.
     */
    default int createTable( final IDBConnection idbConnection,
                             final String tableName,
                             final String tableValues ) throws SQLException
    {
        return idbConnection.createTable(tableName + tableValues);
    }

}
