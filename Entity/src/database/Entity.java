package database;

import persistence.database.Deletable;
import persistence.database.Insertable;
import persistence.database.Searchable;
import persistence.database.Updateable;

/**
 * A database object, which can be inserted, deleted, updated and searched in a database.
 */
public interface Entity
        extends Insertable, Deletable, Updateable, Searchable {}