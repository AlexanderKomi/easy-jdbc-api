package connection.database;

import com.google.gson.Gson;
import connection.database.config.ConnectionDetails;
import connection.database.config.IDataBaseConfig;
import connection.database.config.ILoginData;
import connection.database.statements.PreparedStatements;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Use this class for all persistence.connection.database interactions.
 */
public class DBConnection
        extends AbstractDBConnection
        implements IDBConnection
{

    public static   boolean           AUTO_START_CONNECTION = false;
    protected final ConnectionDetails connectionDetails;

    public DBConnection( final ILoginData loginData,
                         final IDataBaseConfig dataBaseConfig )
            throws SQLException, ClassNotFoundException
    {
        this(
                loginData.getUsername(),
                loginData.getPassword(),
                dataBaseConfig.getDatabaseDriver(),
                dataBaseConfig.getDatabaseConnectionString()
            );
    }

    private DBConnection( final String databaseUserName,
                          final String dataBasePassword,
                          final String databaseDriver,
                          final String databaseURL )
            throws ClassNotFoundException, SQLException
    {

        if ( databaseDriver == null ) {
            throw new NullPointerException("Constructor argument is null: databaseDriver");
        }
        else if ( dataBasePassword == null ) {
            throw new NullPointerException("Constructor argument is null: dataBasePassword");
        }
        else if ( databaseUserName == null ) {
            throw new NullPointerException("Constructor argument is null: databaseUserName");
        }
        else if ( databaseURL == null ) {
            throw new NullPointerException("Constructor argument is null: databaseURL");
        }

        if ( DRIVER_CHECK ) {
            Class.forName(databaseDriver);
        }

        this.connectionDetails = new ConnectionDetails(databaseUserName,
                                                       dataBasePassword,
                                                       databaseURL);

        if ( AUTO_START_CONNECTION ) {
            startConnection();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void startConnection() throws SQLException {

        if ( connection != null ) {
            if ( !connection.isClosed() ) {
                throw new IllegalStateException("Connection is already established.");
            }
        }

        connection = DriverManager.getConnection(
                this.connectionDetails.getDatabaseURL(),
                this.connectionDetails.getDatabaseUserName(),
                this.connectionDetails.getDataBasePassword());

        connection.setAutoCommit(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals( final Object obj ) {
        if ( obj != null ) {
            if ( obj instanceof DBConnection ) {
                final DBConnection temp = (DBConnection) obj;
                return this.connectionDetails.equals(temp.connectionDetails) &&
                       this.connection.equals(temp.getConnection()) &&
                       this.dbStatements.equals(temp.getDBStatements())
                        ;
            }
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "\"dbStatements\":" +
               new Gson().toJson(this.getDBStatements().toString()) + ", " +
               "\"connectionDetails\":" + new Gson().toJson(this.connectionDetails)
                ;
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public PreparedStatements getDBStatements() {
        return dbStatements;
    }

    public ConnectionDetails getConnectionDetails() {
        return connectionDetails;
    }
}
