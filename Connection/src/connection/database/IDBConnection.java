package connection.database;

import connection.database.statements.PreparedStatements;
import persistence.database.*;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface IDBConnection
        extends AutoCloseable
{

    Connection getConnection();

    PreparedStatements getDBStatements();

    /**
     * Checks if correct driver is installed. Otherwise it crashes!
     * @param databaseDriver A String which specified the classpath to the driver.
     * @throws ClassNotFoundException Driver is not installed.
     */
    static void driverCheck( final String databaseDriver ) throws ClassNotFoundException {
        Class.forName(databaseDriver);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    default void close() throws NullPointerException, SQLException, ConnectException {
        if ( getConnection() != null ) {
            if ( !getConnection().isClosed() ) {
                getConnection().close();
            }
            else {
                throw new ConnectException("Connection already closed.");
            }
        }
        else {
            throw new NullPointerException("Connection is null");
        }
    }

    /**
     * Starts the connection to the specified database.
     * @throws SQLException Error send by the database.
     */
    void startConnection()
            throws SQLException;

    /**
     * Asks if the connection to the server is closed.
     * @return Returns true, when the connection is closed.
     * @throws SQLException Error send by the database.
     */
    default boolean isClosed()
            throws SQLException
    {
        return this.getConnection().isClosed();
    }

    /**
     * Sends a sql statement, which should insert stuff or update it.
     *
     * @param update
     *         A SQL String statement for update or insert.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0
     *         for SQL statements that return nothing
     * @throws SQLException Error send by the database.
     * @throws ConnectException Connection is not established.
     */
    default int updateAndCommit( final String update )
            throws SQLException, ConnectException
    {
        final int result = update(update);
        commit();
        return result;
    }

    /**
     * Sends a sql statement, which should insert stuff or update it.
     *
     * @param updateable
     *         An object which implements the {@link Updateable} interface.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0
     *         for SQL statements that return nothing
     *
     * @throws SQLException
     *         Error send by the database.
     * @throws ConnectException
     *         Connection is not established.
     */
    default int updateAndCommit( final Updateable updateable )
            throws SQLException, ConnectException
    {
        return updateAndCommit(
                PrepareStuff.prepareUpdateStatement(
                        updateable.getTableName(),
                        updateable));
    }

    /**
     * Sends a sql query to the server.
     * @return The ResultSet must be parsed by the user. Refer to online guides how to do this
     * (or e.g. StackOverflow).
     * @param query The query send to the server.
     * @throws SQLException Error send by the database.
     */
    default ResultSet query( final String query )
            throws SQLException
    {
        return getDBStatements().query(query,
                                       getConnection());
    }

    /**
     * Creates a table.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     *         or (2) 0 for SQL statements that return nothing
     * @param tableName Table name and values to create it.
     * @throws SQLException Error send by the database.
     */
    default int createTable( final String tableName )
            throws SQLException
    {
        return getDBStatements().createTable(tableName,
                                             getConnection());
    }

    /**
     * Creates a table.
     * @param hasOwnTable An object, which implements the {@link HasOwnTable} interface
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     *         or (2) 0 for SQL statements that return nothing
     * @throws SQLException Error send by the database.
     * */
    default int createTable( final HasOwnTable hasOwnTable )
            throws SQLException
    {
        return createTable(hasOwnTable.getTableName());
    }

    /**
     * Drops a table.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     *         or (2) 0 for SQL statements that return nothing
     * @param tableName Name of the table, which should be dropped.
     * @throws SQLException Error send by the database.
     */
    default int dropTable( final String tableName )
            throws SQLException
    {
        return getDBStatements().dropTable(
                tableName,
                getConnection());
    }

    /**
     * Drops a table.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     *         or (2) 0 for SQL statements that return nothing
     * @param hasOwnTable An object, which implements the {@link HasOwnTable} interface
     * @throws SQLException Error send by the database.
     */
    default int dropTable( final HasOwnTable hasOwnTable )
            throws SQLException
    {
        return dropTable(hasOwnTable.getTableName());
    }

    /**
     * Sends an update statement to the server.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     *         or (2) 0 for SQL statements that return nothing
     * @param update SQL Update Statement.
     * @throws SQLException Error send by the database.
     */
    default int update( final String update )
            throws SQLException
    {
        return getDBStatements().update(update,
                                        getConnection());
    }

    /**
     * Sends an update statement to the server.
     *
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     *         or (2) 0 for SQL statements that return nothing
     * @param updateable An object, which implements the {@link Updateable} interface
     * @throws SQLException Error send by the database.
     */
    default int update( final Updateable updateable )
            throws SQLException
    {
        return getDBStatements().update(
                updateable.getValuesForUpdate(),
                getConnection());
    }

    /**
     * "Select * from " + tableName + " where" + whereStatement
     *
     * @param tableName
     *         SQL Table name
     * @param whereStatement
     *         Part of the sql in the where statement.
     *
     * @return Answer from the database
     *
     * @throws SQLException
     *         Error message from the database.
     */
    default ResultSet search( final String tableName,
                              final String whereStatement )
            throws SQLException
    {
        return query(
                PrepareStuff.prepareSearchStatement(
                        tableName,
                        whereStatement));
    }

    /**
     * "Select * from " + tableName
     *
     * @param tableName
     *         Name of the sql table
     *
     * @return Answer from the database.
     *
     * @throws SQLException
     *         Error message from the database.
     */
    default ResultSet getAll( final String tableName )
            throws SQLException
    {
        return query("Select * from " + tableName);
    }

    /**
     * "Select * from " + tableName
     *
     * @param hasOwnTable
     *         An object, which implements the {@link HasOwnTable} interface
     *
     * @return Answer from the database.
     *
     * @throws SQLException
     *         Error message from the database.
     */
    default ResultSet getAll( final HasOwnTable hasOwnTable )
            throws SQLException
    {
        return getAll(hasOwnTable.getTableName());
    }

    /**
     * SQL Statement: "insert"
     *
     * @param insert
     *         SQL insert statement
     *
     * @return Answer from database
     *
     * @throws SQLException
     *         Error message from the database.
     */
    default int insert( final String insert )
            throws SQLException
    {
        return getDBStatements().insert(insert,
                                        getConnection());
    }

    /**
     * SQL Statement: "insert"
     *
     * @param insertable
     *         An object, which implements the {@link Insertable} interface
     *
     * @return Answer from database
     *
     * @throws SQLException
     *         Error message from the database.
     */
    default int insert( final Insertable insertable )
            throws SQLException
    {
        return insert(
                PrepareStuff.prepareInsertStatement(
                        insertable.getTableName(),
                        insertable));
    }

    /**
     * Performs a rollback and sends it to the database.
     *
     * @throws SQLException
     *         Error message from the database.
     * @throws NullPointerException
     *         Connection is null
     */
    default void rollback()
            throws SQLException, NullPointerException
    {
        if ( getConnection() != null ) {
            this.getConnection().rollback();
        }
        else {
            throw new NullPointerException("Connection is null.");
        }
    }

    /**
     * Sends a "commit" to the database.
     *
     * @throws SQLException
     *         Error message from the database.
     * @throws NullPointerException
     *         Connection is null
     * @throws ConnectException
     *         Connection is not established
     */
    default void commit()
            throws SQLException, NullPointerException, ConnectException
    {
        if ( getConnection() != null ) {
            if ( !getConnection().isClosed() ) {
                getConnection().commit();
            }
            else {
                throw new ConnectException("Connection is already closed.");
            }
        }
        else {
            throw new NullPointerException("Connection is null");
        }
    }
}
