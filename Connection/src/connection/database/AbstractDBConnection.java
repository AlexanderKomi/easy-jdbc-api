package connection.database;

import connection.database.statements.PreparedStatements;

import java.sql.Connection;

abstract public class AbstractDBConnection {
    protected static boolean            DRIVER_CHECK = true;
    protected final  PreparedStatements dbStatements = new PreparedStatements();
    protected        Connection         connection;


    @Override
    public String toString() {
        return "{" +
               "DRIVER_CHECK: \"" + DRIVER_CHECK + "\"," +
               "PreparedStatements: \"" + dbStatements + "\"," +
               "Connection: \"" + connection + "\"" +
               "}";
    }
}
