package connection.database.config;

public interface ILoginData {

    /**
     * Get a String representation of the username entered.
     *
     * @return THe username.
     */
    String getUsername();

    /**
     * A String representation of the password
     * @return password
     * */
    String getPassword();

}
