package connection.database.config;

public interface IDataBaseConfig {

    String getDatabaseConnectionString();

    static String getDatabaseConnectionString(
            final String databaseType,
            final String dataBaseURL,
            final String databasePort,
            final String databaseName )
    {
        return getDatabaseConnectionString(databaseType,
                                           dataBaseURL,
                                           databasePort,
                                           databaseName,
                                           "");
    }

    String getDatabaseType();

    String getDataBaseURL();

    String getDatabasePort();

    String getDatabaseDriver();

    String getDatabaseName();

    static String getDatabaseConnectionString(
            final String databaseType,
            final String dataBaseURL,
            final String databasePort,
            final String databaseName,
            final String extraAppended )
    {
        return "jdbc:" + databaseType + "://" +
               dataBaseURL +
               ":" +
               databasePort +
               "/" +
               databaseName +
               extraAppended;
    }

}
