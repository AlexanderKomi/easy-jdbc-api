package connection.database.config;

public interface IDefaultDatabaseConfig
        extends IDataBaseConfig
{

    default String getDatabaseConnectionString() {
        return IDataBaseConfig.getDatabaseConnectionString(
                this.getDatabaseType(),
                this.getDataBaseURL(),
                this.getDatabasePort(),
                this.getDatabaseName());
    }

}
