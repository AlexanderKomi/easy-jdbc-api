package connection.database.config;

import com.google.gson.Gson;
import persistence.json.JsonDeserializable;
import persistence.json.JsonSerializable;

import java.util.Objects;

public final class ConnectionDetails
        implements JsonSerializable,
                   JsonDeserializable
{
    private final String databaseUserName;
    private final String dataBasePassword;
    private final String databaseURL;

    public ConnectionDetails( final String databaseUserName,
                              final String dataBasePassword,
                              final String databaseURL )
    {
        validateParameters(databaseUserName,
                           dataBasePassword,
                           databaseURL);
        this.databaseUserName = databaseUserName;
        this.dataBasePassword = dataBasePassword;
        this.databaseURL      = databaseURL;
    }


    private void validateParameters(
            final String databaseUserName,
            final String dataBasePassword,
            final String databaseURL )
    {
        boolean             allParametersValid = true;
        final StringBuilder errorMessage       = new StringBuilder();
        if ( dataBasePassword == null ) {
            allParametersValid = false;
            errorMessage.append("Constructor Argument is null: dataBasePassword\n");
        }
        else if ( databaseUserName == null ) {
            allParametersValid = false;
            errorMessage.append("Constructor Argument is null: databaseUserName\n");
        }
        else if ( databaseURL == null ) {
            allParametersValid = false;
            errorMessage.append("Constructor Argument is null: databaseURL\n");
        }

        if ( !allParametersValid ) {
            throw new NullPointerException(errorMessage.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return 31 *
               ( Objects.hashCode(databaseUserName) +
                 Objects.hashCode(dataBasePassword) +
                 Objects.hashCode(databaseURL) );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals( final Object obj ) {
        if ( obj != null ) {
            if ( obj instanceof ConnectionDetails ) {
                final ConnectionDetails c = (ConnectionDetails) obj;
                return c.databaseUserName.equals(this.databaseUserName) &&
                       c.databaseURL.equals(this.databaseURL) &&
                       c.dataBasePassword.equals(this.dataBasePassword);
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.toJson();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toJson() {
        return new Gson().toJson(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings( "unchecked" )
    public <T extends JsonDeserializable> T fromJson( final String json ) {
        return (T) new Gson().fromJson(json,
                                       ConnectionDetails.class);
    }

    //------------------------------ GETTER AND SETTER ------------------------------

    public String getDatabaseUserName() {
        return databaseUserName;
    }

    public String getDataBasePassword() {
        return dataBasePassword;
    }

    public String getDatabaseURL() {
        return databaseURL;
    }
}