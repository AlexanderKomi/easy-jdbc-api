package connection.database.config;

import java.util.Objects;

public class DataBaseConfig
        implements IDefaultDatabaseConfig
{

    private final String dataBaseURL;
    private final String databaseDriver;
    private final String databaseType;
    private final String databasePort;
    private final String databaseName;

    public DataBaseConfig(
            final String databaseType,
            final String databaseDriver,
            final String databaseName,
            final String dataBaseURL,
            final String databasePort )
    {
        nullChecks(databaseType,
                   databaseDriver,
                   databaseName,
                   dataBaseURL,
                   databasePort);
        this.databaseType   = databaseType;
        this.databaseDriver = databaseDriver;
        this.databasePort   = databasePort;
        this.databaseName   = databaseName;
        this.dataBaseURL    = dataBaseURL;
    }

    private void nullChecks( final String databaseType,
                             final String databaseDriver,
                             final String databaseName,
                             final String dataBaseURL,
                             final String databasePort )
    {
        boolean             allAreNotNull = true;
        final StringBuilder errorMessages = new StringBuilder();
        if ( databasePort == null ) {
            allAreNotNull = false;
            errorMessages.append("Database port is null.\n");
        }
        if ( databaseType == null ) {
            allAreNotNull = false;
            errorMessages.append("Database type is null.\n");
        }
        if ( databaseDriver == null ) {
            allAreNotNull = false;
            errorMessages.append("Database driver is null.\n");
        }
        if ( databaseName == null ) {
            allAreNotNull = false;
            errorMessages.append("Database name is null.\n");
        }
        if ( dataBaseURL == null ) {
            allAreNotNull = false;
            errorMessages.append("Database url is null.\n");
        }

        if ( !allAreNotNull ) {
            throw new NullPointerException(errorMessages.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(dataBaseURL,
                            databaseDriver,
                            databaseType,
                            databasePort,
                            databaseName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals( final Object obj ) {
        if ( obj != null ) {
            if ( obj instanceof DataBaseConfig ) {
                final DataBaseConfig temp = (DataBaseConfig) obj;
                return this.dataBaseURL.equals(temp.dataBaseURL) &&
                       this.databaseType.equals(temp.databaseType) &&
                       this.databaseName.equals(temp.databaseName) &&
                       this.databaseDriver.equals(temp.databaseDriver) &&
                       this.databasePort.equals(temp.databasePort)
                        ;
            }
        }
        return false;
    }

    //-------------------------- GETTER AND SETTER --------------------------

    public String getDatabaseType() {
        return databaseType;
    }

    public String getDataBaseURL() {
        return dataBaseURL;
    }

    public String getDatabasePort() {
        return databasePort;
    }

    public String getDatabaseDriver() {
        return databaseDriver;
    }

    public String getDatabaseName() {
        return databaseName;
    }
}