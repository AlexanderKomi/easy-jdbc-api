package connection.database.config;

import java.util.Objects;

public class LoginData implements ILoginData {

    private final String username;
    private final String password;

    public LoginData() {
        this.username = "root";
        this.password = "";
    }

    public LoginData( final String username,
                      final String password )
    {
        this.username = username;
        this.password = password;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(getUsername(),
                            getPassword());
    }

    /**{@inheritDoc}*/
    @Override
    public boolean equals( final Object o ) {
        if ( this == o ) { return true; }
        if ( !( o instanceof LoginData ) ) { return false; }
        final LoginData loginData = (LoginData) o;
        return Objects.equals(getUsername(),
                              loginData.getUsername()) &&
               Objects.equals(getPassword(),
                              loginData.getPassword());
    }

    /**{@inheritDoc}*/
    @Override
    public String getUsername() {
        return username;
    }

    /**{@inheritDoc}*/
    @Override
    public String getPassword() {
        return password;
    }
}
