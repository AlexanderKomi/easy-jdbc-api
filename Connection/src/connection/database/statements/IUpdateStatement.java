package connection.database.statements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface IUpdateStatement {

    /**
     * Sending update to database using provided connection.
     * @param query SQL Query send to the database.
     * @param connection Connection used to send sql.
     * @return if statement was successful, returns 0
     * @throws SQLException Error send by the database.
     */
    default int update( final String query,
                        final Connection connection ) throws SQLException
    {
        try {
            setPreparedStatement(IPreparedStatements.prepareStatement(query,
                                                                      connection,
                                                                      getPreparedStatement()));
            return getPreparedStatement().executeUpdate();
        }
        catch ( final SQLException sqlException ) {
            connection.rollback();
            throw sqlException;
        }
    }

    PreparedStatement getPreparedStatement();

    void setPreparedStatement( final PreparedStatement preparedStatement );
}
