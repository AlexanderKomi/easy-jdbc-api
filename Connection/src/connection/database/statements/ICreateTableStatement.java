package connection.database.statements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface ICreateTableStatement {

    String CREATE_STATEMENT = "CREATE TABLE if not exists ";

    /**
     * "CREATE TABLE if not exists tableName"
     * @param tableName See statement above
     * @param connection Connection used to send sql.
     * @return if statement was successful, returns 0
     * @throws SQLException Error send by the database.
     * */
    default int createTable( final String tableName,
                             final Connection connection ) throws SQLException
    {
        try {
            setPreparedStatement(
                    IPreparedStatements.prepareStatement(
                            CREATE_STATEMENT + tableName,
                            connection,
                            getPreparedStatement()));
            return getPreparedStatement().executeUpdate();
        }
        catch ( final SQLException sqlException ) {
            connection.rollback();
            throw sqlException;
        }
    }

    PreparedStatement getPreparedStatement();

    void setPreparedStatement( final PreparedStatement preparedStatement );
}
