package connection.database.statements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface IPreparedStatements {

    /**
     * Prepares statement before sending to database.
     * Makes null checks and creates a new prepared statement, when it is already closed.
     * @param sql The sql to create the prepared statement.
     * @param connection Connection is required to create a prepared statement.
     * @param preparedStatement This preparedStatement can be changed, when supplied to this
     *                          function!
     *                          It is newly assigned, if it is null oder closed.
     * @return A new and fresh prepared statement.
     * @throws SQLException Error send by the database.
     */
    static PreparedStatement prepareStatement( final String sql,
                                               final Connection connection,
                                               PreparedStatement preparedStatement )
            throws SQLException
    {
        if ( preparedStatement == null ) {
            preparedStatement = prepareStatement(sql,
                                                 connection);
        }
        else {
            if ( !preparedStatement.isClosed() ) {
                preparedStatement.close();
            }
            preparedStatement = prepareStatement(sql,
                                                 connection);
        }
        return preparedStatement;
    }

    /**
     * Prepares statement before sending to database. Makes null checks and creates a new prepared
     * statement, when it is already closed.
     *
     * @param sql
     *         The sql to create the prepared statement.
     * @param connection
     *         Connection is required to create a prepared statement.
     *
     * @return A new and fresh prepared statement.
     * @throws SQLException Error send by the database.
     */
    static PreparedStatement prepareStatement(
            final String sql,
            final Connection connection )
            throws SQLException
    {
        boolean             parametersAreNotNull = true;
        final StringBuilder errorMessages        = new StringBuilder();
        if ( sql == null ) {
            parametersAreNotNull = false;
            errorMessages.append("SQL Statement is null");
        }
        else if ( connection == null ) {
            parametersAreNotNull = false;
            errorMessages.append("Connection is null");
        }
        if ( !parametersAreNotNull ) {
            throw new NullPointerException(errorMessages.toString());
        }

        if ( connection.isClosed() ) {
            throw new IllegalStateException("Connection already closed.");
        }

        return connection.prepareStatement(sql);
    }

}
