package connection.database.statements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface IQueryStatement {

    /**
     * Connects to the database and executes the parameter query.
     *
     * @param connection Connection used to send the query
     * @param query
     *         An sql query to the database
     *
     * @return Result of the query
     *
     * @author Alexander Komischke
     * @author Adrian Görisch
     * @throws SQLException Error send by the database.
     * @throws IllegalStateException Connection is closed.
     */
    default ResultSet query( final String query,
                             final Connection connection )
            throws SQLException, IllegalStateException
    {
        if ( connection == null ) {
            throw new NullPointerException("Connection is null.");
        }
        else if ( connection.isClosed() ) {
            throw new IllegalStateException("Connection is closed.");
        }
        setPreparedStatement(
                IPreparedStatements.prepareStatement(
                        query,
                        connection,
                        getPreparedStatement()));
        return getPreparedStatement().executeQuery();
    }

    PreparedStatement getPreparedStatement();

    void setPreparedStatement( final PreparedStatement preparedStatement );

}
