package connection.database.statements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface IDropTableStatement {

    String DROP_STATEMENT = "drop table if exists ";

    /**
     * "drop table if exists tableName"
     * @param tableName See sql above.
     * @param connection Connection used to send sql.
     * @return if statement was successful, returns 0.
     * @throws SQLException Error send by the database.
     *
     * */
    default int dropTable( final String tableName,
                           final Connection connection ) throws SQLException
    {
        try {
            setPreparedStatement(IPreparedStatements.prepareStatement(
                    DROP_STATEMENT + tableName,
                    connection,
                    getPreparedStatement()));
            return getPreparedStatement().executeUpdate();
        }
        catch ( final SQLException sqlException ) {
            connection.rollback();
            throw sqlException;
        }
    }

    PreparedStatement getPreparedStatement();

    void setPreparedStatement( final PreparedStatement preparedStatement );
}
