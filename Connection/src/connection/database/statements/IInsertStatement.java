package connection.database.statements;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface IInsertStatement {

    /**
     * "insertStatement"
     *
     * @param insertStatement
     *         See sql statement above
     * @param connection
     *         Connection used to send sql.
     *
     * @return if statement was successful, returns 0
     *
     * @throws SQLException
     *         Error send by the database.
     */
    default int insert( final String insertStatement,
                        final Connection connection )
            throws SQLException
    {
        try {
            setPreparedStatement(IPreparedStatements.prepareStatement(insertStatement,
                                                                      connection,
                                                                      getPreparedStatement()));
            return getPreparedStatement().executeUpdate();
        }
        catch ( final SQLException sqlException ) {
            connection.rollback();
            throw sqlException;
        }
    }

    PreparedStatement getPreparedStatement();

    void setPreparedStatement( final PreparedStatement preparedStatement );

}
