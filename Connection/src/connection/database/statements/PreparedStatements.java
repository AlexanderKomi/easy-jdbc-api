package connection.database.statements;

import java.sql.PreparedStatement;
import java.util.Objects;

/**
 * Useful tools for persistence.connection.database interaction.
 *
 * @author Alex
 */
public class PreparedStatements implements
                                IQueryStatement,
                                IUpdateStatement,
                                IDropTableStatement,
                                ICreateTableStatement,
                                IInsertStatement
{

    private PreparedStatement preparedStatement;

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public void setPreparedStatement( final PreparedStatement preparedStatement ) {
        this.preparedStatement = preparedStatement;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPreparedStatement());
    }

    @Override
    public boolean equals( final Object o ) {
        if ( this == o ) { return true; }
        if ( !( o instanceof PreparedStatements ) ) { return false; }
        final PreparedStatements that = (PreparedStatements) o;
        return Objects.equals(getPreparedStatement(),
                              that.getPreparedStatement());
    }
}
