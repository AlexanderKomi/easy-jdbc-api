package persistence.json;

import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;

public interface JsonDeserializable {

    /**
     * Constructs an object from a given json string.
     * @param <T> Some type, which is JsonDeserializable
     * @param json Json which contains a single, or multiple Objects.
     *             When multiple Objects are in the json, please make sure that type T is
     *             correctly specified.
     * @return An instance of type T
     */
    @SuppressWarnings( "unchecked" )
    default <T extends JsonDeserializable> T fromJson( final String json ) {
        return (T) new Gson().fromJson(json,
                                       ( (ParameterizedType) getClass()
                                               .getGenericSuperclass()
                                       ).getActualTypeArguments()[0]);
    }
}
