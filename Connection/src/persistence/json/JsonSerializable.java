package persistence.json;

import java.util.ArrayList;

public interface JsonSerializable {

    /**
     * When called, this object formats itself into a json string. Can not be default implemented,
     * because gson-instances are preserved and finalized, when implementing this interface !
     *
     * @return json string
     */
    String toJson();

    static <T extends JsonSerializable> String toJson( final ArrayList<T> many ) {
        final String            linebreak = "\n";
        final ArrayList<String> results   = new ArrayList<>();
        many.forEach(abstimmung -> results.add(abstimmung.toJson()));

        final StringBuilder sb = new StringBuilder("{");

        for ( final String s : results ) {
            sb.append(s).append(linebreak);
        }
        sb.append("}");
        return sb.toString();
    }

}
