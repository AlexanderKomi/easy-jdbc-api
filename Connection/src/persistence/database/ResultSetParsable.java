package persistence.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ResultSetParsable extends HasOwnTable {

    /**
     * Every entity has a parse method. When parsing a ResultSet, multiple T can be found, but only
     * one should be found. This method checks a parsed result and returns the only valid value.
     *
     * @param parsingResult
     *         The parsing method of an subclass of entity should return an ArrayList and supply it
     *         to this method.
     *
     * @param <T> Return type inherent from ResultSetParsable
     *
     * @return The checked ArrayList should only contain a single valid value. Otherwise an
     *         IllegalArgument Exception is thrown.
     *
     * @author Alexander Komischke
     */
    static <T extends ResultSetParsable> T checkParsedResultSet( final ArrayList<T> parsingResult )
            throws IllegalArgumentException, NullPointerException
    {
        if ( parsingResult == null ) {
            throw new NullPointerException("checkParsedResultSet(resultSet) returns null.");
        }
        if ( parsingResult.isEmpty() ) {
            throw new IllegalArgumentException("List parameter is empty.");
        }
        if ( parsingResult.size() > 1 ) {
            throw new IllegalArgumentException("List contains multiple Entity.");
        }

        return parsingResult.get(0);
    }

    ArrayList<Object> parseResultSet( final ResultSet databaseAnswer )
            throws SQLException;
}
