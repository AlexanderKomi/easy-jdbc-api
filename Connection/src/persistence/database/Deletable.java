package persistence.database;

import connection.database.IDBConnection;

import java.net.ConnectException;
import java.sql.SQLException;

public interface Deletable extends HasOwnTable {

    default int delete( final IDBConnection exampleConnection )
            throws SQLException, ConnectException
    {
        return Deletable.delete(exampleConnection,
                                this);
    }

    static <T extends Deletable> int delete( final IDBConnection idbConnection,
                                             final T entity )
            throws SQLException, ConnectException, NullPointerException
    {
        return delete(idbConnection,
                      entity.getValuesForDelete(),
                      entity);
    }

    static <T extends Deletable> int delete( final IDBConnection idbConnection,
                                             final String whereCondition,
                                             final T entity )
            throws SQLException, ConnectException, NullPointerException
    {
        if ( entity == null ) {
            throw new NullPointerException("entity parameter is null");
        }
        if ( whereCondition == null ) {
            throw new NullPointerException("The string of the where-condition is null.");
        }
        if ( idbConnection == null ) {
            throw new NullPointerException("IDBConnection is null");
        }
        return idbConnection.updateAndCommit(
                PrepareStuff.prepareDeleteStatement(whereCondition,
                                                    entity));
    }

    /**
     * Syntactically correct representation of the "Delete from tableName where ... " statement of
     * a delete.
     *
     * @return Replaces the ... with the result of getValuesForDelete(), when delete() is used.
     *
     * @author Alexander Komischke
     */
    String getValuesForDelete();

}
