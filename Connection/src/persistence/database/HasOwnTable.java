package persistence.database;

public interface HasOwnTable {

    /**
     * @return Returns table name from persistence.connection.database as String
     */
    String getTableName();
}
