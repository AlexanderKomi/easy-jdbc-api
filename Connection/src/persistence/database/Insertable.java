package persistence.database;

import connection.database.IDBConnection;

import java.net.ConnectException;
import java.sql.SQLException;

public interface Insertable
        extends HasOwnTable
{


    /**
     * {@inheritDoc}
     */
    default int insert( final IDBConnection exampleConnection )
            throws SQLException, ConnectException
    {
        return insertAndCommit(exampleConnection,
                               this);
    }

    /**
     * Inserts a value to the connection.database.
     *
     * @param idbConnection Connection used to send the insert.
     * @param entity
     *         The entity, which should be inserted to the connection.database. Uses the
     *         getValuesForInsert() method of an entity.
     *
     * @return Lines changed in the connection.database. Useful for error handling. By default,
     *         returns -1, when an error occures.
     *
     * @author Alexander Komischke
     * @throws SQLException Error send by the database
     * @throws ConnectException Connection is not established
     * @throws NullPointerException Some of the function parameters are null.
     */
    static int insertAndCommit( final IDBConnection idbConnection,
                                final Insertable entity )
            throws SQLException, ConnectException, NullPointerException
    {
        if ( entity == null ) {
            throw new NullPointerException("Entity is null.");
        }
        if ( idbConnection == null ) {
            throw new NullPointerException("IDBConnection is null.");
        }
        return idbConnection.updateAndCommit(
                PrepareStuff.prepareInsertStatement(entity.getTableName(),
                                                    entity));
    }

    /**
     * Syntactically correct representation of the "VALUES ... " statement of an insert.
     *
     * @return Replaces the ... with the result of getValuesForInsert(), when insert() is used.
     *
     * @author Alexander Komischke
     */
    String getValuesForInsert();
}
