package persistence.database;

import connection.database.IDBConnection;

import java.net.ConnectException;
import java.sql.SQLException;

/**
 * The Class implementing this interface, has an own table and can be updated in a connection.database.
 *
 * @author Alexander Komischke
 */
public interface Updateable extends HasOwnTable {

    default int update( final IDBConnection exampleConnection )
            throws SQLException, ConnectException
    {
        return update(exampleConnection,
                      getValuesForUpdate());
    }

    default int update( final IDBConnection exampleConnection,
                        final String whereCondition )
            throws SQLException,
                   ConnectException, NullPointerException
    {
        return update(exampleConnection,
                      whereCondition,
                      this);
    }

    /**
     * Just a proxy function for {@link PrepareStuff}.prepareUpdateStatement.
     *
     * "Update updateable.getTableName() Set updateable.getValuesForUpdate() Where whereCondition"
     *
     * @param whereCondition
     *         Conditions for the update.
     * @param updateable
     *         Object, which inherits the {@link Updateable}Updateable interface.
     *
     * @return A well formatted sql String.
     */
    static String prepareUpdateStatement( final String whereCondition,
                                          final Updateable updateable )
    {
        return PrepareStuff.prepareUpdateStatement(whereCondition,
                                                   updateable);
    }

    /**
     * Syntactically correct representation of the "Set ... " statement of a update.
     *
     * @return Replaces the ... with the result of getValuesForUpdate(), when update() is
     *         used.
     *
     * @author Alexander Komischke
     */
    String getValuesForUpdate();

    static int update( final IDBConnection idbConnection,
                       final String whereCondition,
                       final Updateable updateable )
            throws SQLException, ConnectException, NullPointerException
    {
        if ( idbConnection != null ) {
            return idbConnection.updateAndCommit(PrepareStuff.prepareUpdateStatement(whereCondition,
                                                                                     updateable));
        }
        throw new NullPointerException("IDBConnection is null ");
    }


}
