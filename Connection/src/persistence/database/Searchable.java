package persistence.database;

import connection.database.IDBConnection;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Searchable {

    /**
     * "Select * from " + tableName
     * <p>
     * Get all entries from a table
     *
     *
     * @param connection Connection used to send the query.
     * @param tableName
     *         the name of the table
     *
     * @return A ResultSet with all entries the database found
     *
     * @throws SQLException
     *         Error send by the database.
     */
    static ResultSet getAll( final IDBConnection connection,
                             final String tableName ) throws SQLException
    {
        return connection.query("Select * from " + tableName);
    }

    //----------------------- search -----------------------

    /**
     * "SELECT * FROM table_name Where searchString"
     *
     * @param connection
     *         Connection used to send the query.
     * @param table_name
     *         Name of the table, where something is searched
     * @param searchString
     *         Where-Condition for search.
     *
     * @return Answer what the database found.
     *
     * @throws SQLException
     *         Error send by the database.
     */
    static ResultSet searchLike( final IDBConnection connection,
                                 final String table_name,
                                 final String searchString )
            throws SQLException
    {
        return connection.query(PrepareStuff.prepareSearchStatement(table_name,
                                                                    searchString));
    }

    /**
     * For each i in columnNames: "SELECT * FROM table_name Where lower(searchString) like
     * lower(columnNames[i]) or ..."
     *
     * @param connection
     *         Connection used to send the query.
     * @param table_name
     *         Name of the table, where something is searched
     * @param searchString
     *         Where-Condition for search.
     * @param columnNames
     *         See SQL above.
     *
     * @return Answer what the database found.
     *
     * @throws SQLException
     *         Error send by the database.
     */
    static ResultSet searchLike( final IDBConnection connection,
                                 final String table_name,
                                 final String searchString,
                                 final String... columnNames )
            throws SQLException
    {
        return connection.query(
                PrepareStuff.prepareSearchStatement(
                        table_name,
                        PrepareStuff.formatForQuery(
                                searchString,
                                columnNames)));
    }

}
