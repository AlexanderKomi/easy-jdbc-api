package persistence.database;

public class PrepareStuff {

    /**
     * "Update updateable.getTableName() Set updateable.getValuesForUpdate() Where whereCondition"
     *
     * @param whereCondition
     *         Conditions for the update.
     * @param updateable
     *         Object, which inherits the {@link Updateable}Updateable interface.
     *
     * @return A well formatted sql String.
     */
    public static String prepareUpdateStatement( final String whereCondition,
                                                 final Updateable updateable )
    {
        return prepareUpdateStatement(whereCondition,
                                      updateable.getTableName(),
                                      updateable.getValuesForUpdate());
    }

    /**
     * "Update updateable.getTableName() Set updateable.getValuesForUpdate() Where whereCondition"
     *
     * @param whereCondition
     *         Conditions for the update.
     * @param tableName
     *         Name of the table, where entries should be updated.
     * @param valuesForUpdate
     *         Which values should be updated.
     *
     * @return A well formatted sql String.
     */
    public static String prepareUpdateStatement(
            final String whereCondition,
            final String tableName,
            final String valuesForUpdate )
    {
        return String.format("Update %s Set %s Where %s",
                             tableName,
                             valuesForUpdate,
                             whereCondition);
    }

    /**
     * SELECT * FROM table_name Where whereCondition
     *
     * @param table_name
     *         In which table the data should be searched.
     * @param whereCondition
     *         Conditions for the search.
     *
     * @return A well formatted sql String.
     */
    public static String prepareSearchStatement( final String table_name,
                                                 final String whereCondition )
    {
        if ( table_name == null ) {
            throw new NullPointerException("Table name parameter is null.");
        }
        if ( whereCondition == null ) {
            throw new NullPointerException("Where condition parameter is null.");
        }
        return String.format("Select * From %s Where %s",
                             table_name,
                             whereCondition);
    }

    /**
     * "Insert into tableName Values insertable.getValuesForInsert()"
     *
     * @param tableName
     *         In which table the data should be insert.
     * @param insertable
     *         Supplies conditions for the insert.
     *
     * @return A well formatted sql String.
     */
    public static String prepareInsertStatement( final String tableName,
                                                 final Insertable insertable )
    {
        return prepareInsertStatement(tableName,
                                      insertable.getValuesForInsert());
    }

    /**
     * "Insert into tableName Values valuesForInsert"
     *
     * @param tableName
     *         In which table the data should be insert.
     * @param valuesForInsert
     *         Supplies conditions for the insert.
     *
     * @return A well formatted sql String.
     */
    public static String prepareInsertStatement( final String tableName,
                                                 final String valuesForInsert )
    {
        if ( tableName == null ) {
            throw new NullPointerException("Table name parameter is null.");
        }
        if ( valuesForInsert == null ) {
            throw new NullPointerException("Where condition parameter is null.");
        }
        return String.format("Insert into %s Values (%s)",
                             tableName,
                             valuesForInsert);
    }

    /**
     * "Delete From entity.getTableName() Where whereCondition"
     *
     * @param whereCondition
     *         Conditions for the deletion.
     * @param deletable
     *         Supplies table name.
     *
     * @return A well formatted sql String.
     */
    public static String prepareDeleteStatement( final String whereCondition,
                                                 final Deletable deletable )
    {
        return prepareDeleteStatement(whereCondition,
                                      deletable.getTableName());
    }

    /**
     * "Delete From tableName Where whereCondition"
     *
     * @param whereCondition
     *         Conditions for the deletion.
     * @param tableName
     *         Name of the table
     *
     * @return A well formatted sql String.
     */
    public static String prepareDeleteStatement( final String whereCondition,
                                                 final String tableName )
    {
        return String.format("Delete From %s Where %s",
                             tableName,
                             whereCondition);
    }

    public static String formatForQuery( final String searchString,
                                         final String... columnNames )
    {
        final String replacedSearchString = searchString.replace("'",
                                                                 "\'\'");

        final StringBuilder res = new StringBuilder();
        for ( int i = 0 ; i < columnNames.length ; i++ ) {
            res.append("lower(")
               .append(columnNames[i])
               .append(") like lower('%")
               .append(replacedSearchString)
               .append("%') ");
            if ( i != columnNames.length - 1 ) {
                res.append(" or ");
            }
        }
        return res.toString();
    }
}
