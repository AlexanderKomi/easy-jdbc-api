package connection.database;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.ConnectException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MySQLConnectionTest {

    private static final String TEST_DATABASE_NAME = "test";

    private static final String table_name = "`actor` (\n" +
                                             "  `a_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT primary key,\n" +
                                             "  `a_vorname` varchar(50) DEFAULT NULL,\n" +
                                             "  `a_nachname` varchar(50) DEFAULT NULL\n" +
                                             ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    static void getAll2() {
        MySQLConnection m = null;
        try {
            m = new MySQLConnection(TEST_DATABASE_NAME);
            m.startConnection();
            m.getAll(table_name);
        }
        catch ( final SQLException | ClassNotFoundException e ) {
            e.printStackTrace();
        }
        finally {
            try {
                assert m != null;
                m.close();
            }
            catch ( SQLException | ConnectException e ) {
                e.printStackTrace();
            }
        }
    }

    @BeforeAll
    @Test
    static void createTable() {
        MySQLConnection m = null;
        try {
            m = new MySQLConnection(TEST_DATABASE_NAME);
            m.startConnection();
            m.createTable(table_name);
        }
        catch ( final SQLException | ClassNotFoundException e ) {
            e.printStackTrace();
        }
        finally {
            try {
                if ( m != null ) {
                    m.close();
                }
            }
            catch ( SQLException | ConnectException e ) {
                e.printStackTrace();
            }
        }
    }

    @AfterAll
    @Test
    static void dropTable() {
        MySQLConnection m = null;
        try {
            m = new MySQLConnection(TEST_DATABASE_NAME);
            m.dropTable(table_name);
            Assertions.assertThrows(SQLSyntaxErrorException.class,
                                    MySQLConnectionTest::getAll2);
        }
        catch ( final SQLException | ClassNotFoundException e ) {
            e.printStackTrace();
        }
        finally {
            try {
                assert m != null;
                m.close();
            }
            catch ( SQLException | ConnectException e ) {
                e.printStackTrace();
            }
        }

    }

    @Test
    void getAll() {

        MySQLConnection m = null;
        try {
            m = new MySQLConnection(TEST_DATABASE_NAME);
            m.startConnection();
            ResultSet r    = m.getAll(table_name);
            int       size = 0;
            while ( r.next() ) {
                size++;
            }
            assertEquals(0,
                         size);

        }
        catch ( final SQLException | ClassNotFoundException e ) {
            e.printStackTrace();
        }
        finally {
            try {
                assert m != null;
                m.close();
            }
            catch ( SQLException | ConnectException e ) {
                e.printStackTrace();
            }
        }

    }

    @Test
    void toString1() {
        DBConnection.AUTO_START_CONNECTION = true;
        MySQLConnection exampleDBConnection = null;
        try {
            System.out.println("Trying to initialize Connection");
            exampleDBConnection = new MySQLConnection("root",
                                                      "",
                                                      "Test");
            System.out.printf("Connected to connection.database: %s%n",
                              exampleDBConnection);

        }
        catch ( SQLException | ClassNotFoundException e ) {
            e.printStackTrace();
        }
        finally {
            try {
                if ( exampleDBConnection != null ) {
                    exampleDBConnection.close();
                }
            }
            catch ( SQLException | ConnectException e ) {
                e.printStackTrace();
            }
        }
    }
}