package connection.database;

import connection.database.config.DataBaseConfig;
import connection.database.config.IDataBaseConfig;

public class MySQLDataBaseConfig extends DataBaseConfig implements IMySQLDataBaseConfig {

    MySQLDataBaseConfig(
            final String databaseName,
            final String dataBaseURL,
            final String databasePort )
    {
        super(Defaults.TYPE,
              Defaults.DEFAULT_DRIVER,
              databaseName,
              dataBaseURL,
              databasePort);
    }

    public MySQLDataBaseConfig( final String databaseName ) {
        super(Defaults.TYPE,
              Defaults.DEFAULT_DRIVER,
              databaseName,
              Defaults.LOCAL_HOST_URL,
              Defaults.PORT);
    }

    @Override
    public String getDatabaseConnectionString() {
        return IDataBaseConfig.getDatabaseConnectionString(
                this.getDatabaseType(),
                this.getDataBaseURL(),
                this.getDatabasePort(),
                this.getDatabaseName()) + Defaults.timeStampErrorPrevention;
    }
}
