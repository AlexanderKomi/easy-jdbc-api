package connection.database;

import connection.database.config.IDataBaseConfig;

public interface IMySQLDataBaseConfig extends IDataBaseConfig {

    final class Defaults {
        static final String TYPE                     = "mysql";
        static final String DEFAULT_DRIVER           = "com.mysql.cj.jdbc.Driver";
        static final String USERNAME                 = "root";
        static final String PASSWORD                 = "";
        static final String LOCAL_HOST_URL           = "localhost";
        static final String PORT                     = "3306";
        static final String timeStampErrorPrevention =
                "?useUnicode=true&useJDBCCompliantTimezoneShift" +
                "=true" +
                "&useLegacyDatetimeCode=false&serverTimezone=UTC";
    }
    @Override
    default String getDatabaseConnectionString() {
        return IDataBaseConfig.getDatabaseConnectionString(
                this.getDatabaseType(),
                this.getDataBaseURL(),
                this.getDatabasePort(),
                this.getDatabaseName()) + Defaults.timeStampErrorPrevention;
    }

}
