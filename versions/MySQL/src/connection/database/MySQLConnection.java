package connection.database;

import connection.database.config.ILoginData;
import connection.database.config.LoginData;

import java.sql.SQLException;

public class MySQLConnection
        extends DBConnection
{

    public MySQLConnection( final String username,
                            final String password,
                            final String databaseName )
            throws SQLException, ClassNotFoundException
    {
        this(new LoginData(
                     username,
                     password),
             new MySQLDataBaseConfig(
                     databaseName,
                     IMySQLDataBaseConfig.Defaults.LOCAL_HOST_URL,
                     IMySQLDataBaseConfig.Defaults.PORT));
    }

    public MySQLConnection( final ILoginData loginData,
                            final IMySQLDataBaseConfig dataBaseConfig )
            throws SQLException, ClassNotFoundException
    {
        super(loginData,
              dataBaseConfig);
    }

    public MySQLConnection( final String username,
                            final String password,
                            final String databaseName,
                            final String dataBaseUrl,
                            final String dataBasePort )
            throws SQLException, ClassNotFoundException
    {
        this(new LoginData(
                     username,
                     password),
             new MySQLDataBaseConfig(
                     databaseName,
                     dataBaseUrl,
                     dataBasePort));
    }

    public MySQLConnection( final String databaseName )
            throws SQLException, ClassNotFoundException
    {
        this(new LoginData(
                     IMySQLDataBaseConfig.Defaults.USERNAME,
                     IMySQLDataBaseConfig.Defaults.PASSWORD
             ),
             new MySQLDataBaseConfig(
                     databaseName,
                     IMySQLDataBaseConfig.Defaults.LOCAL_HOST_URL,
                     IMySQLDataBaseConfig.Defaults.PORT)
            );
    }
}
