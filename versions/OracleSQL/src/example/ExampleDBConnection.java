package example;

import connection.database.OracleSQLConnection;
import connection.database.OracleSQLDataBaseConfig;
import connection.database.config.LoginData;

import java.sql.SQLException;

public class ExampleDBConnection {

    public static void main( String[] args ) {

        LoginData loginData = new LoginData("",
                                            "");
        OracleSQLDataBaseConfig oracleSQLDataBaseConfig = new OracleSQLDataBaseConfig("db01",
                                                                                      "3306");
        System.out.println(oracleSQLDataBaseConfig.getDatabaseConnectionString());
        try {
            OracleSQLConnection oracleSQLConnection = new OracleSQLConnection(loginData,
                                                                              oracleSQLDataBaseConfig);
            oracleSQLConnection.startConnection();
        }
        catch ( SQLException e ) {
            e.printStackTrace();
        }
        catch ( ClassNotFoundException e ) {
            e.printStackTrace();
        }
    }
}
