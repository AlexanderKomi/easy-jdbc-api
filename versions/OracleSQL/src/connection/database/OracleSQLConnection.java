package connection.database;

import connection.database.config.IDataBaseConfig;
import connection.database.config.ILoginData;

import java.sql.SQLException;

public class OracleSQLConnection
        extends DBConnection
        implements IOracleSQLConnection
{

    public OracleSQLConnection( final ILoginData loginData,
                                final IDataBaseConfig dataBaseConfig )
            throws SQLException, ClassNotFoundException
    {
        super(loginData,
              dataBaseConfig);
    }
}
