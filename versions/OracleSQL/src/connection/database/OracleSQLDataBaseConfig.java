package connection.database;

import connection.database.config.DataBaseConfig;

public class OracleSQLDataBaseConfig extends DataBaseConfig implements IOracleSQLDataBaseConfig {

    public OracleSQLDataBaseConfig(
            final String databaseName,
            final String databasePort )
    {
        this(databaseName,
             Defaults.LOCALHOST,
             databasePort);
    }

    public OracleSQLDataBaseConfig(
            final String databaseName,
            final String dataBaseURL,
            final String databasePort )
    {
        super(Defaults.TYPE,
              Defaults.DEFAULT_DRIVER,
              databaseName,
              dataBaseURL,
              databasePort);
    }


}
