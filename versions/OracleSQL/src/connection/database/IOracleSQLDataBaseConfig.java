package connection.database;

import connection.database.config.IDataBaseConfig;

public interface IOracleSQLDataBaseConfig extends IDataBaseConfig {

    final class Defaults {
        static final String LOCALHOST      = "localhost";
        static final String TYPE           = "oracle:thin";
        static final String DEFAULT_DRIVER = "oracle.jdbc.driver.OracleDriver";
        static final String DATABASE_NAME  = ":db01";
    }

}
